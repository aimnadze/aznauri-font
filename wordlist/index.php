<?php

$left_chars = [
    '"', '/numbersign', '&', '\'', '(', '*', '+', ',', '-', '.',
    '7', ':', ';', '=', '?',
    'c', 'f', 'l', 'p', 't', 'y',
    'C', 'F', 'L', 'P', 'T', 'Y',
    '/bracketleft', '^', '_', '{', '}', '~',
    'г', 'д', 'р', 'с', 'т', 'у', 'ц', 'щ', 'ъ', 'ь',
    'Г', 'Д', 'Р', 'С', 'Т', 'У', 'Ц', 'Щ', 'Ъ', 'Ь',
    'ა', 'ბ', 'გ', 'დ', 'ზ', 'ლ', 'პ',
    'რ', 'ს', 'ღ', 'ჩ', 'წ', 'ხ', 'ჯ',
];

$right_chars = [
    '"', '/numbersign', '&', '\'', ')', '*', '+', ',', '-', '.',
    '1', '3', '4', '7', ':', ';', '=', '?',
    'j', 't', 'y',
    'J', 'T', 'Y',
    '/bracketright', '^', '_', '{', '}', '~',
    'д', 'з', 'л', 'т', 'у', 'ч', 'ъ', 'э',
    'Д', 'З', 'Л', 'Т', 'У', 'Ч', 'Ъ', 'Э',
    'ა', 'ბ', 'გ', 'ე', 'ვ', 'ზ', 'კ', 'პ',
    'ჟ', 'უ', 'ფ', 'შ', 'ძ', 'ჭ', 'ჯ', 'ჰ',
];

$array = [];
foreach ($left_chars as $left_char) {
    foreach ($right_chars as $right_char) {
        $array[] = "$left_char$right_char";
    }
}

file_put_contents('index.txt', join("\n", $array));
