<?php

function check_case ($chars) {

    $lowercase = [

        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z',

        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж',
        'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
        'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц',
        'ч', 'ш', 'щ', 'ь', 'ы', 'ъ', 'э', 'ю',
        'я',

        'á', 'à', 'â', 'ã', 'ä', 'å', 'ă', 'æ', 'č', 'ç', 'ď', 'é', 'è',
        'ê', 'ë', 'ě', 'ğ', 'ì', 'í', 'ï', 'ĺ', 'ľ', 'ñ', 'ó',
        'ô', 'õ', 'ő', 'œ', 'ŕ', 'ř', 'š', 'ť', 'ù', 'ü', 'ů', 'ý', 'ÿ',
        'ž',

        'ѝ', 'љ', 'ј',

    ];

    $uppercase = [

        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z',

        'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж',
        'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О',
        'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц',
        'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю',
        'Я',

        'Á', 'À', 'Â', 'Ã', 'Ä', 'Å', 'Ă', 'Æ', 'Č', 'Ç', 'Ď', 'É', 'È',
        'Ê', 'Ë', 'Ě', 'Ğ', 'Ì', 'Í', 'Ï', 'Ĺ', 'Ľ', 'Ñ', 'Ó',
        'Ô', 'Õ', 'Ő', 'Œ', 'Ŕ', 'Ř', 'Š', 'Ť', 'Ù', 'Ü', 'Ů', 'Ý', 'Ÿ',
        'Ž',

        'Ѝ', 'Љ', 'Ј',

    ];

    $similars = [
        ['0', 'o', 'о'],
        ['3', 'ვ'],
        ['a', 'а'],
        ['b', 'в'],
        ['c', 'с'],
        ['e', 'е'],
        ['ë', 'ё'],
        ['h', 'н'],
        ['i', '|'],
        ['j', 'ј'],
        ['k', 'к'],
        ['m', 'м'],
        ['p', 'р'],
        ['t', 'т'],
        ['x', 'х'],
        ['y', 'у'],
    ];

    foreach ($lowercase as $i => $char) {

        $upper_char = $uppercase[$i];
        if (!array_key_exists($upper_char, $chars)) {
            die("ERROR: Char $upper_char uppercase of $char not found\n");
        }

        include_once __DIR__ . '/check_kerning.php';
        check_kerning($char, $upper_char, $chars);

        include_once __DIR__ . '/check_spline_set.php';
        check_spline_set($char, $upper_char, $chars);

    }

    foreach ($uppercase as $i => $char) {

        $lower_char = $lowercase[$i];
        if (!array_key_exists($lower_char, $chars)) {
            die("ERROR: Char $lower_char lowercase of $char not found\n");
        }

        include_once __DIR__ . '/check_kerning.php';
        check_kerning($char, $lower_char, $chars);

        include_once __DIR__ . '/check_spline_set.php';
        check_spline_set($char, $lower_char, $chars);

    }

    foreach ($similars as $similar_chars) {
        foreach ($similar_chars as $i => $char) {

            if (!array_key_exists($char, $chars)) {
                die("ERROR: Char $char not found\n");
            }

            if ($i === 0) continue;

            include_once __DIR__ . '/check_kerning.php';
            check_kerning($char, $similar_chars[0], $chars);

            include_once __DIR__ . '/check_spline_set.php';
            check_spline_set($char, $similar_chars[0], $chars);

        }
    }

}
