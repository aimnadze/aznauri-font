<?php

function check_advance ($char, $char_data) {
    foreach ($char_data['portable_kerns2'] as $kern_char => $triple) {

        $advance = (int)$triple[1];

        if (!in_array($advance, [-100, -200, -300])) {
            die("ERROR: Wierd advance $advance on $char$kern_char\n");
        }

        $max_advance = floor($char_data['width'] / 2 / 100) * 100;
        if (-$advance > $max_advance) {
            die("ERROR: Advance $advance on $char$kern_char too low. Should be at least -$max_advance\n");
        }

    }
}
