<?php

function check_kerning ($char, $remote_char, $chars) {

    $kerns2 = $chars[$char]['portable_kerns2'];
    $remote_kerns2 = $chars[$remote_char]['portable_kerns2'];

    foreach ($kerns2 as $kern_char => $triple) {

        if (!array_key_exists($kern_char, $remote_kerns2)) {
            die("ERROR: Kerning $remote_char$kern_char not found while $char$kern_char found\n");
        }

        if ($triple !== $remote_kerns2[$kern_char]) {
            die("ERROR: Kerning $remote_char$kern_char differ from $char$kern_char\n");
        }

    }

}
